# Orange book

A fork of the [Legrand Orange book](https://www.latextemplates.com/template/the-legrand-orange-book).
Color can be modified (named **primary**) in the orangebook.cls class.